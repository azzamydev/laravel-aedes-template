<?php

use App\Http\Livewire\Pages;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', Pages\Dashboard::class)->name('dashboard');
Route::get('/forms', Pages\Forms::class)->name('forms');
Route::get('/inputs', Pages\Input::class)->name('inputs');
