<?php
namespace App\Traits;

trait HasBreadcrumb
{
    protected function getBreadcrumb() : array 
    {
        return [
            [
                'label' => 'Forms',
                'link' => route('forms')
            ]
        ];
    }
}