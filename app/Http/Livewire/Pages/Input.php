<?php

namespace App\Http\Livewire\Pages;

use Livewire\Component;
use App\Traits\HasBreadcrumb;

class Input extends Component
{
    use HasBreadcrumb;

    public function render()
    {
        return view('livewire.pages.input');
    }

    protected function getBreadcrumb(): array
    {
        return [
            [
                'label' => 'Inputs',
                'link' => route('inputs')
            ]
        ];
    }
}
