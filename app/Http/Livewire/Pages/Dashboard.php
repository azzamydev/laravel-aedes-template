<?php

namespace App\Http\Livewire\Pages;

use App\Traits\HasBreadcrumb;
use Livewire\Component;

class Dashboard extends Component
{
    use HasBreadcrumb;

    public function render()
    {
        return view('livewire.pages.dashboard');
    }

    protected function getBreadcrumb(): array
    {
        return [
            [
                'label' => 'Dashboard',
                'link' => route('dashboard')
            ]
        ];
    }
}
