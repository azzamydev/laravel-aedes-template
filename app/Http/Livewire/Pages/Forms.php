<?php

namespace App\Http\Livewire\Pages;

use Livewire\Component;
use App\Traits\HasBreadcrumb;

class Forms extends Component
{
    use HasBreadcrumb;
    
    public function render()
    {
        return view('livewire.pages.forms');
    }

    protected function getBreadcrumb(): array
    {
        return [
            [
                'label' => 'Forms',
                'link' => route('forms')
            ]
        ];
    }
}
