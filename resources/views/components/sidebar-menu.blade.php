@props(['name' => '-'])
<section class="my-3 w-full text-gray-700 dark:text-slate-400">
    <p x-cloak x-show="@js($name !== '-')" class="dark:bg-slate-950/30 mb-2 w-full bg-slate-100 px-6 py-2">
        {{$name}}
    </p>
    <ul class="w-full space-y-1 px-2">
        {{$slot}}
    </ul>
</section>