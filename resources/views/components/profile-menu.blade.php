<div class="relative flex-shrink-0"
    x-data="profile_menu"
    x-on:click.outside="open = false">
    <button class="group relative flex" x-on:click="toggle()">
        <span>
            <img class="h-9 w-9 rounded-full object-cover saturate-50 group-hover:saturate-100"
                src="/images/user-profile.jpeg" alt="image" />
        </span>
    </button>
    <ul x-cloak x-show="open"
        x-transition
        x-transition.duration.300ms
        class="absolute top-11 w-[230px] z-50 overflow-hidden rounded-lg border bg-white !py-0 text-sm text-gray-700 shadow-xl ltr:right-0 rtl:left-0 dark:border-0 dark:bg-slate-700 dark:text-gray-400">
        <li>
            <div class="flex items-center px-4 py-4">
                <div class="flex-none">
                    <img class="h-10 w-10 rounded-md object-cover"
                        src="/images/user-profile.jpeg"
                        alt="image" />
                </div>
                <div class="ltr:pl-4 rtl:pr-4">
                    <h4 class="text-base">John Doe<span
                            class="bg-primary-300 rounded px-1 text-xs text-gray-700 ltr:ml-2 rtl:ml-2">Pro</span>
                    </h4>
                    <a class=""
                        href="javascript:;">johndoe@gmail.com</a>
                </div>
            </div>
        </li>
        <li class="flex">
            <a href="/users/profile" class="w-full space-x-3 px-4 py-2 hover:bg-slate-100 hover:dark:bg-slate-800 dark:hover:text-white" x-on:click="toggle()">
                <i class="bi bi-person"></i>
                Profile
            </a>
        </li>
        <li class="flex justify-start">
            <button x-on:click="$store.darkMode.toggle()" class="flex w-full gap-x-2 px-4 py-2 text-start hover:bg-slate-100 hover:dark:bg-slate-800 dark:hover:text-white">
                <i x-cloak x-show="!$store.darkMode.on" class="bi bi-sun-fill"></i>
                <i x-cloak x-show="$store.darkMode.on" class="bi bi-moon-stars-fill"></i>
                Dark Mode
            </button>
        </li>
        <li class="flex border-t">
            <a href="/users/profile" class="w-full space-x-3 px-4 py-3 text-red-500 hover:bg-slate-100 hover:dark:bg-slate-800 dark:hover:text-white" x-on:click="toggle()">
                <i class="bi bi-lock-fill"></i>
                Sign Out
            </a>
        </li>
    </ul>
</div>
@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('profile_menu', () => ({
                open: false,

                init() {},
                toggle() {
                    this.open = !this.open
                },
            }))
        })
    </script>
@endpush
