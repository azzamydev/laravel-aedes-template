<nav class="flex min-h-[60px] w-full justify-between gap-3 border-b bg-white px-3 py-3 dark:border-b-gray-700 dark:bg-gray-900/80 md:px-5">

    {{-- Left --}}
    <div class="flex items-center gap-3">
        <x-brand class="hidden md:mr-10 md:flex" />
        <button x-on:click="$store.sidebar.toggle()">
            <i :class="$store.sidebar.on ? 'bi bi-chevron-double-left text-xl' : 'bi bi-list text-2xl'"></i>
        </button>
    </div>

    @php
        $data = [
            [
                'readed' => false,
                'title' => 'Lorem ipsum dolor sit.',
                'icon' => 'bi-chat-left-text-fill text-green-500',
                'message' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, harum.',
            ],
            [
                'readed' => false,
                'title' => 'Lorem ipsum dolor sit.',
                'icon' => 'bi-clock-fill text-yellow-500',
                'message' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Recusandae, harum.',
            ],
        ];
    @endphp
    {{-- Right --}}
    <section class="flex items-center gap-x-5">
        <x-notification :data="$data" />
        <x-profile-menu />
    </section>

</nav>
