@props([
    'name' => '',
    'label' => '',
    'inline' => false,
    'message' => '',
    'color' => 'danger',
])
<div class="{{ $inline ? 'flex-row items-start gap-x-3' : 'flex-col' }} {{ $message !== '' && $color == 'danger' ? 'has-error' : '' }} {{ $message !== '' && $color == 'success' ? 'has-success' : '' }} flex">
    <label for="{{ $name }}" class="mb-1 ml-1 capitalize text-gray-600 dark:text-gray-400">{{ $label }}</label>
    <div class="w-full">
        <input id="{{ $name }}" name="{{ $name }}" type="radio" {{ $attributes->merge(['class' => 'form-radio ']) }}></input>
        @if ($message !== '')
            <span class="text-{{ $color }}-500 ml-1 text-[11px]">{{ $message }}</span>
        @endif
    </div>
</div>
