@props([
    'icon' => '',
    'iconColor' => '',
])

<button
    {{ $attributes->merge(['class' => 'btn flex items-center gap-2 rounded-full']) }}>
    @if ($icon != '')
        @php
            $colorClass = '';
            switch ($iconColor) {
                case 'success':
                    $colorClass = 'text-success-500';
                    break;
                case 'secondary':
                    $colorClass = 'text-secondary-500';
                    break;
                case 'danger':
                    $colorClass = 'text-danger-500';
                    break;
                case 'warning':
                    $colorClass = 'text-warning-500';
                    break;
                case 'info':
                    $colorClass = 'text-info-500';
                    break;
            
                default:
                    $colorClass = '';
                    break;
            }
        @endphp
        <i class="text-base bi {{ $icon }} {{ $colorClass }}"></i>
    @endif
    {{ $slot }}
</button>
