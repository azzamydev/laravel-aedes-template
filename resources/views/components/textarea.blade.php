@props([
    'name' => '',
    'label' => '',
    'type' => 'text',
    'inline' => false,
    'message' => '',
    'color' => 'danger',
])
<div class="{{ $inline ? 'flex-row items-start gap-x-3' : 'flex-col' }} {{ $message !== '' && $color == 'danger' ? 'has-error' : '' }} {{ $message !== '' && $color == 'success' ? 'has-success' : '' }} flex">
    <label for="{{ $name }}" class="{{ !$inline ?: 'w-full max-w-[120px] mt-2' }} mb-1 ml-1 capitalize text-gray-600 dark:text-gray-400">{{ $label }}</label>
    <div class="w-full">
        <textarea id="{{ $name }}" name="{{ $name }}" type="{{ $type }}" {{ $attributes->merge(['class' => 'form-input w-full']) }}></textarea>
        @if ($message !== '')
            <span class="text-{{ $color }}-500 ml-1 text-[11px]">{{ $message }}</span>
        @endif
    </div>
</div>
