@props([
    'name' => '',
    'label' => '',
    'type' => 'text',
    'inline' => false,
    'message' => '',
    'color' => 'danger',
])
<div class="{{ $inline ? 'flex-row items-start gap-x-3' : 'flex-col' }} {{ $message !== '' && $color == 'danger' ? 'has-error' : '' }} {{ $message !== '' && $color == 'success' ? 'has-success' : '' }} flex">
    <label for="{{ $name }}" class="{{ !$inline ?: 'w-full max-w-[120px] mt-2' }} mb-1 ml-1 capitalize text-gray-600 dark:text-gray-400">{{ $label }}</label>
    @if ($type !== 'password')
        <div class="w-full">
            <input name="{{ $name }}" id="{{ $name }}" type="{{ $type }}" {{ $attributes->merge(['class' => 'form-input w-full']) }}>
            @if ($message !== '')
                <span class="text-{{ $color }}-500 ml-1 text-[11px]">{{ $message }}</span>
            @endif
        </div>
    @else
        <div class="w-full">
            <div class="relative w-full" x-data="{
                open: false
            }">
                <input name="{{ $name }}" id="{{ $name }}" :type="open ? 'text' : 'password'" {{ $attributes->merge(['class' => 'form-input w-full']) }}>
                <div class="absolute inset-y-0 right-3 flex items-center justify-center">
                    <button x-on:click="open = !open">
                        <i class="bi dark:text-gray-400" :class="open ? 'bi-eye-fill' : 'bi-eye-slash-fill'"></i>
                    </button>
                </div>
            </div>
            @if ($message !== '')
                <span class="text-{{ $color }}-500 ml-1 text-[11px]">{{ $message }}</span>
            @endif
        </div>
    @endif
</div>
