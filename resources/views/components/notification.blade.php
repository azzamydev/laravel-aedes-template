@props(['data' => []])

<div class="relative"
    x-data="notification">
    <button
        x-on:click="open = true"
        class="flex h-[37px] w-[37px] items-center justify-center rounded-full bg-slate-100 dark:bg-gray-800/80">
        <i class="bi bi-bell hover:text-primary-500 hoverdark::text-primary-300 duration-200"></i>
        <span class="absolute top-1 flex h-3 w-3 ltr:right-0 rtl:left-0">
            <span
                class="absolute -top-[3px] inline-flex h-full w-full animate-ping rounded-full bg-green-500/50 opacity-75 ltr:-left-[3px] rtl:-right-[3px]"></span>
            <span class="relative inline-flex h-[6px] w-[6px] rounded-full bg-green-500"></span>
        </span>
    </button>
    <ul x-cloak
        class="fixed inset-y-0 z-50 w-full overflow-hidden max-w-sm border bg-white !py-0 text-sm text-gray-700 shadow-xl transition duration-200 ltr:right-0 rtl:left-0 dark:border-0 dark:bg-slate-800 dark:text-gray-400"
        :class="open ? 'translate-x-0' : 'translate-x-full'">
        <li class="flex items-center justify-between px-4 pt-3">
            <h3 class="text-sm md:text-base">Notifikasi</h3>
            <i x-on:click="open =false" class="bi bi-x-lg cursor-pointer"></i>
        </li>
        <li class="flex items-center gap-3 border-b px-4 py-2 text-xs dark:border-gray-500">
            <button class="hover:underline">Tandai sudah dibaca</button>
            <span>|</span>
            <button class="hover:underline">Bersihkan</button>
        </li>

        <li x-show="notifications.length == 0" class="flex h-2/3 flex-col items-center justify-center gap-3">
            <i class="bi bi-x-circle text-3xl text-yellow-500"></i>
            <p>Tidak ada notifikasi</p>
        </li>

        <template x-for="(notif, index) in notifications">
            <li 
            x-on:click="setRead(index)"
            class="flex items-center gap-5 px-4 py-3 border-b dark:border-gray-500"
            :class="!notif.readed && 'bg-slate-100 dark:bg-slate-700/50 cursor-pointer'">
                <i class="bi text-xl" :class="notif.icon"></i>
                <div>
                    <p x-text="notif.title"></p>
                    <p class="text-xs text-gray-500" x-text="notif.message"></p>
                </div>
                <i x-on:click="" class="bi bi-x-circle cursor-pointer"></i>
            </li>
        </template>
    </ul>
</div>
@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('notification', () => ({
                open: false,
                notifications: @js($data),

                init() {

                },
                toggle() {
                    this.open = !this.open
                },
                setRead(index){
                    this.notifications[index].readed = true
                }
            }))
        })
    </script>
@endpush
