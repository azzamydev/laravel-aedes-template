<div {{ $attributes->merge(['class' => 'shadow bg-white dark:bg-slate-800 rounded-lg p-5']) }}>
    {{$slot}}
</div>
