<section x-cloak class="flex items-center text-primary-500 justify-center gap-3 text-center">
    <i class="bi bi-apple text-2xl"></i>
    <h1
        class="text-2xl font-bold">{{ config('app.name') }}</h1>
</section>
