@props([
    'label' => '-',
    'disableIcon' => false,
    'active' => false,
    'icon' => 'bi-circle-fill',
    'link' => 'javascript:;',
])
<li class="">
    <a href="{{ $link }}"
        class="{{ $active ? 'bg-slate-100 dark:bg-slate-800' : 'hover:bg-slate-100 hover:dark:bg-slate-800' }} group flex items-center gap-3 rounded-md px-3 py-2 duration-150 hover:cursor-pointer">
        <i class="bi {{ $icon }} {{ $active ? 'text-primary-600 dark:text-primary-400' : 'group-hover:text-primary-600 dark:group-hover:text-primary-400' }}"
            x-cloak :class="@js($disableIcon) && 'hidden'"></i>
        <span>{{ $label }}</span>
    </a>
</li>
