@props(['name' => '-', 'active' => false])

<li class="" x-data="{
    open: @js($active)
}">
    <div x-on:click="open = !open"
        class="group flex items-center justify-between gap-3 rounded-md px-3 py-2 duration-150 hover:cursor-pointer hover:bg-slate-100 hover:dark:bg-slate-800">
        <span x-show="$store.sidebar.on">{{ $name }}</span>
        <i x-show="$store.sidebar.on" class="bi bi-chevron-down duration-200" :class="open ? 'rotate-180' : 'rotate-0'"></i>
    </div>
    <ul x-cloak x-show="open" x-collapse class="w-full py-1 px-1 space-y-1">
        {{ $slot }}
    </ul>
</li>
