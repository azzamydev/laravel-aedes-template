<section x-cloak x-show="!$store.sidebar.on"
    {{ $attributes->merge(['class' => 'flex text-primary-500 items-center justify-center gap-3 text-center']) }}>
    <i class="bi bi-apple md:text-2xl"></i>
    <h1 class="text-base font-bold md:text-2xl">{{ config('app.name') }}</h1>
</section>
