@props(['list' => []])

<ol x-cloak class="flex font-medium text-gray-500 dark:text-white/80">
    @foreach ($list as $key => $item)
        @if ($key == 0)
            <li>
                <a href="{{ $item['link'] }}">
                    {{ $item['label'] }}
                </a>
            </li>
        @else
            @if ($key !== count($list) - 1)
                <li class="before:px-1.5 before:content-['/']">
                    <a href="{{ $item['link'] }}"
                        class="">
                        {{ $item['label'] }}
                    </a>
                </li>
            @else
                <li class="before:px-1.5 before:content-['/']">
                    <a href="{{ $item['link'] }}"
                        class="text-black hover:text-black/70 dark:text-white dark:hover:text-white/70">
                        {{ $item['label'] }}
                    </a>
                </li>
            @endif
        @endif
    @endforeach
</ol>
