<aside x-cloak
    class="absolute z-50 h-full bg-white py-5 text-[15px] drop-shadow-xl duration-200 dark:bg-gray-900 md:relative"
    x-bind:class="$store.sidebar.on ? 'translate-x-0 w-[280px]' : '-translate-x-[280px] w-[0px]'">

    <x-brand-side />

    <x-sidebar-menu>
        <x-sidebar-menu.item
            icon="bi-house-fill"
            label="Dasboard"
            :active="Route::is('dashboard')"
            link="{{ route('dashboard') }}" />
        <x-sidebar-menu.item
            icon="bi-ui-radios"
            label="Forms"
            :active="Route::is('forms')"
            link="{{ route('forms') }}" />
        <x-sidebar-menu.item
            icon="bi-input-cursor-text"
            label="Inputs"
            :active="Route::is('inputs')"
            link="{{ route('inputs') }}" />
        <x-sidebar-menu.item-dropdown name="Components">
            <x-sidebar-menu.item
                disableIcon
                label="Example"
                link="javascript:;" />
            <x-sidebar-menu.item
                disableIcon
                label="Example"
                link="javascript:;" />
        </x-sidebar-menu.item-dropdown>
    </x-sidebar-menu>

</aside>
