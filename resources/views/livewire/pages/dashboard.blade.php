<div x-data="dashboard">
    <x-slot:title>Dashboard</x-slot:title>
    <x-breadcrumb :list="$this->getBreadcrumb() ?: []" />
</div>
@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('dashboard', () => ({
                init() {
                    console.log('sip')
                }
            }))
        })
    </script>
@endpush
