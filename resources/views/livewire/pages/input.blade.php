<div x-data="inputs">
    <x-slot:title>Inputs</x-slot:title>
    <x-breadcrumb :list="$this->getBreadcrumb() ?: []" />

    <div class="grid grid-cols-1 gap-5 p-5 md:grid-cols-2 lg:grid-cols-3">
        <x-card class="space-y-5">
            <h1>Text Input</h1>
            <x-text-input name="textinput" label="Text" placeholder="Enter here" />
            <x-text-input name="textinput" label="Text" placeholder="Enter here" />
            <x-text-input name="password" label="Password" type="password" placeholder="Enter your password" />
            <x-textarea name="textare" label="Textarea" placeholder="Enter here" />
            <x-rupiah name="currency_" label="Rupiah" placeholder="Enter here" />
            <p x-text="$store.currency_"></p>
        </x-card>
        <x-card class="space-y-5">
            <h1>Checkbox & Radio</h1>
            <x-checkbox inline name="checkbox" label="Checkbox" placeholder="Enter here" />
            <x-radio inline name="radio" label="Radio" placeholder="Enter here" />
            <x-checkbox name="checkbox" label="Checkbox" placeholder="Enter here" />
            <x-radio name="radio" label="Radio" placeholder="Enter here" />

        </x-card>
        <x-card class="space-y-5">
            <h1>Button</h1>
            <div class="grid grid-cols-2 gap-3">
                <x-button class="btn-primary">
                    Button
                </x-button>
                <x-button class="btn-success">
                    Button
                </x-button>
                <x-button class="btn-danger">
                    Button
                </x-button>
                <x-button class="btn-warning">
                    Button
                </x-button>
                <x-button class="btn-secondary">
                    Button
                </x-button>
                <x-button class="btn-info">
                    Button
                </x-button>
            </div>
            <h1>Button + Icon</h1>
            <div class="grid grid-cols-2 gap-3">
                <x-button icon="bi-arrow-down-circle" class="btn-primary rounded-lg">
                    Button
                </x-button>
                <x-button icon="bi-cart4" class="btn-success rounded-lg">
                    Button
                </x-button>
            </div>

        </x-card>
    </div>
</div>
@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('inputs', () => ({
                init() {
                    console.log('sip')
                }
            }))
        })
    </script>
@endpush
