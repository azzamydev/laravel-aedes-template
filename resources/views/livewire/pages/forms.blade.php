<div x-data="forms">
    <x-slot:title>Forms</x-slot:title>
    <x-breadcrumb :list="$this->getBreadcrumb() ?: []" />

    <div class="grid grid-cols-1 gap-5 p-5 md:grid-cols-2">
        <x-card class="space-y-5">
            <x-text-input name="name" label="Name" placeholder="Enter your name" />
            <x-text-input color="success" name="email" label="Email Address" type="email" placeholder="Enter your email address" />
            <x-text-input name="password" label="Password" type="password" placeholder="Enter your password" />
            <x-button class="w-full btn-primary">
                Submit
            </x-button>
        </x-card>
        <x-card class="hidden flex-col space-y-5 md:flex">
            <x-text-input inline name="name" label="Name" placeholder="Enter your name" />
            <x-text-input inline name="email" label="Email Address" type="email" placeholder="Enter your email address" />
            <x-text-input inline name="password" label="Password" type="password" placeholder="Enter your password" />
            <div class="flex grow items-end">
                <x-button class="w-full btn-primary">
                    Submit
                </x-button>
            </div>
        </x-card>
        <x-card class="space-y-5">
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." name="name" label="Name" placeholder="Enter your name" />
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." name="email" label="Email Address" type="email" placeholder="Enter your email address" />
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." name="password" label="Password" type="password" placeholder="Enter your password" />
            <x-button class="w-full btn-primary">
                Submit
            </x-button>
        </x-card>
        <x-card class="space-y-5">
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." color="success" name="name" label="Name" placeholder="Enter your name" />
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." color="success" name="email" label="Email Address" type="email" placeholder="Enter your email address" />
            <x-text-input message="Lorem ipsum dolor sit amet consectetur." color="success" name="password" label="Password" type="password" placeholder="Enter your password" />
            <x-button class="w-full btn-primary">
                Submit
            </x-button>
        </x-card>
    </div>

</div>
@push('scripts')
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('forms', () => ({
                init() {
                    console.log('sip')
                }
            }))
        })
    </script>
@endpush
