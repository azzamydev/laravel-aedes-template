<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="ltr">

<head>
    <meta charset='utf-8' />
    <meta name="application-name" content="{{ config('app.name') }}">
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name') . ' - ' . $title ?: '-' }}
    </title>

    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <link rel="icon" href="/favicon.ico" />

    @vite(['resources/css/app.css', 'resources/js/app.js'])
    @livewireStyles
    @livewireScripts
</head>

<body x-cloak class="flex h-screen" x-data="main"
    x-bind:class="[$store.darkMode.on ? 'dark' : 'light']">

    <x-sidebar />
    <div x-cloak x-show="$store.sidebar.on" class="fixed inset-0 z-40 backdrop-blur-sm md:hidden"
        x-on:click="$store.sidebar.close()"></div>
    <section class="w-full">
        <x-navbar />
        <main class="p-5">
            {{ $slot }}
        </main>
    </section>

    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('main', () => ({

            }))
        })
    </script>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.store('darkMode', {

                on: Alpine.$persist(true).as('darkMode_on'),

                toggle() {
                    this.on = !this.on
                }
            })
        })
    </script>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.store('sidebar', {

                on: Alpine.$persist(true).as('sidebar'),

                toggle() {
                    this.on = !this.on
                },
                close() {
                    this.on = false
                },
            })
        })
    </script>
    @stack('scripts')
    @livewire('notifications')
</body>

</html>
