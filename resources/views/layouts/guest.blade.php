<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset='utf-8' />
    <meta name="application-name" content="{{ config('app.name') }}">
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ $title ?? config('app.name') }}
    </title>

    <meta name='viewport' content='width=device-width, initial-scale=1' />
    <link rel="icon" href="/favicon.ico" />

    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body x-data="main">

    <main>
        {{ $slot }}
    </main>
    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('main', () => ({

            }))
        })
    </script>
     @livewire('notifications')
</body>

</html>
