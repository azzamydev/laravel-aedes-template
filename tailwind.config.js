/** @type {import('tailwindcss').Config} */
import colors from "tailwindcss/colors";

module.exports = {
    darkMode: "class",
    content: [
        "./resources/**/*.blade.php",
        "./resources/**/*.js",
        "./resources/**/*.css",
    ],
    theme: {
        extend: {
            colors: {
                primary: {
                    50: "#ecf5ff",
                    100: "#ddedff",
                    200: "#c2ddff",
                    300: "#9cc5ff",
                    400: "#75a1ff",
                    500: "#4271ff",
                    600: "#3656f5",
                    700: "#2a45d8",
                    800: "#253cae",
                    900: "#263a89",
                    950: "#161f50",
                },
                success: {
                    50: "#f0fdf4",
                    100: "#dcfce7",
                    200: "#bbf7d0",
                    300: "#86efac",
                    400: "#4ade80",
                    500: "#22c55e",
                    600: "#16a34a",
                    700: "#15803d",
                    800: "#166534",
                    900: "#14532d",
                    950: "#052e16",
                },
                warning: {
                    50: "#fffbeb",
                    100: "#fef3c7",
                    200: "#fde68a",
                    300: "#fcd34d",
                    400: "#fbbf24",
                    500: "#f59e0b",
                    600: "#d97706",
                    700: "#b45309",
                    800: "#92400e",
                    900: "#78350f",
                    950: "#451a03",
                },
                danger: {
                    50: "#fef2f2",
                    100: "#fee2e2",
                    200: "#fecaca",
                    300: "#fca5a5",
                    400: "#f87171",
                    500: "#ef4444",
                    600: "#dc2626",
                    700: "#b91c1c",
                    800: "#991b1b",
                    900: "#7f1d1d",
                    950: "#450a0a",
                },
                secondary: colors.gray,
                info: colors.cyan,
                dark: "#0f172a",
                light: "#fff"
            },
        },
    },
    plugins: [],
};
